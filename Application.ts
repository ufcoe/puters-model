import {BelongsToMany, DCSerializable, ForeignColumn, Table, Column, ColumnTypes, HasMany} from '@devctrl/data-model';
import {Hostname} from './Hostname';
import {ApplicationDatabase} from './ApplicationDatabase';
import {AppGroupApplication} from './AppGroupApplication';
import {Database} from './Database';
import {AppGroup} from './AppGroup';
import {Note} from "./Note";

@Table
export class Application extends DCSerializable {
  @Column({
    label: "URL Path",
    tooltip: "URL for application root"
  }) urlPath: string;
  @Column({
    label: "Application Type",
    tooltip: "eg. Wordpress, Angular, etc."
  }) appType: string;
  @Column({
    type: ColumnTypes.text,
    label: "Description",
    tooltip: "Description of application instance"
  }) description: string;
  @ForeignColumn(() => Hostname) hostname: Hostname;
  @BelongsToMany(() => Database, () => ApplicationDatabase) databases: Database[];
  @BelongsToMany(() => AppGroup, () => AppGroupApplication) appGroups: AppGroup[];
  @HasMany(() => Note) notes: Note[];

}
