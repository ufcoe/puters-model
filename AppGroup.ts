import {BelongsToMany, DCSerializable, HasMany, Table} from '@devctrl/data-model';
import {AppGroupApplication} from './AppGroupApplication';
import {Application} from './Application';

@Table
export class AppGroup extends DCSerializable {
  @BelongsToMany(() => Application, () => AppGroupApplication) applications: Application[];
}
