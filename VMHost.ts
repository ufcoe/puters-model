import {Table, Column, DCSerializable, HasMany} from '@devctrl/data-model';
import {Machine} from './Machine';
import {Note} from "./Note";


@Table
export class VMHost extends DCSerializable {
    @Column({
        label: "Description"
    }) description: string;
    @HasMany(() => Machine) machines: Machine[];
  @HasMany(() => Note) notes: Note[];
  @Column({
    label: "Cluster Name",
    optional: true
  }) cluster: string;
}
