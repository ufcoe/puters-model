import {DCSerializable, ForeignColumn, Table} from '@devctrl/data-model';
import {AppGroup} from './AppGroup';
import {Application} from './Application';

@Table
export class AppGroupApplication extends DCSerializable {
  @ForeignColumn(() => AppGroup) appGroup: AppGroup;
  @ForeignColumn(() => Application) application: Application;
}
