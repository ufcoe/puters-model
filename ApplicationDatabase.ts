import {DCSerializable, ForeignColumn, Table} from '@devctrl/data-model';
import {Database} from './Database';
import {Application} from './Application';

@Table
export class ApplicationDatabase extends DCSerializable {
  get name() {
    if (this._name) return this._name;

    let dbName = this.database ? this.database.name : "null";
    let appName = this.application ? this.application.name : "null";

    return `${appName}:${dbName}`;
  }

  set name(val) {
    // just ignore set operations, as otherwise updates to the component items won't be reflected
    // this._name = val;
  }

  @ForeignColumn(() => Database) database: Database;
  @ForeignColumn(() => Application) application: Application;
}
