import {Column, ColumnTypes, DCSerializable, ForeignColumn, Table} from "@devctrl/data-model";
import {Machine} from "./Machine";
import {Application} from "./Application";
import {Database} from "./Database";
import {VMHost} from "./VMHost";
import {Hostname} from "./Hostname";
import {IPAddress} from "./IPAddress";

@Table
export class Note extends DCSerializable {
  @Column({ type: ColumnTypes.text}) text: string;
  @Column author: string;
  @ForeignColumn(() => Machine, true) machine: Machine;
  @ForeignColumn(() => Application, true) application: Application;
  @ForeignColumn(() => Database, true) database: Database;
  @ForeignColumn(() => VMHost, true) vmHost: VMHost;
  @ForeignColumn(() => Hostname, true) hostname: Hostname;
  @ForeignColumn(() => IPAddress, true) ipAddress: IPAddress;
}