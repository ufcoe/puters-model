import {DCSerializable, ForeignColumn, Table, HasMany, Column, ColumnTypes} from '@devctrl/data-model';
import {VMHost} from './VMHost';
import {IPAddress} from './IPAddress';
import {Note} from "./Note";

@Table
export class Machine extends DCSerializable {
  @Column({
    type: ColumnTypes.string,
    label: "OS",
    tooltip: "Host operating system"
  }) os: string;
  @ForeignColumn(() => VMHost) vmHost: VMHost;
  @HasMany(() => IPAddress) IPAddresses: IPAddress[];
  @HasMany(() => Note) notes: Note[];
  @Column({
    label: "Active"
  }) active: boolean;
  @Column({
    label: "Icinga"
  }) icinga: boolean;
  @Column({
    optional: true,
    label: "MAC",
    tooltip: "Primary MAC address"
  }) macAddress: string;
  @Column({
    optional: true,
    label: "VMID",
    tooltip: "Proxmox VMID"
  }) vmid: string;
}
