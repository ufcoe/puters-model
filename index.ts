import {AppGroup} from './AppGroup';
import {AppGroupApplication} from './AppGroupApplication';
import {Application} from './Application';
import {ApplicationDatabase} from './ApplicationDatabase';
import {Database} from './Database';
import {Hostname} from './Hostname';
import {IPAddress} from './IPAddress';
import {Machine} from './Machine';
import {VMHost} from './VMHost';
import {Note} from './Note';

export * from './AppGroup';
export * from './Application';
export * from './AppGroupApplication';
export * from './ApplicationDatabase';
export * from './Database';
export * from './Hostname';
export * from './IPAddress';
export * from './Machine';
export * from './Note';
export * from './VMHost';

export const PutersModelList = [
  AppGroup,
  AppGroupApplication,
  Application,
  ApplicationDatabase,
  Database,
  Hostname,
  IPAddress,
  Machine,
  Note,
  VMHost
];

export const modelVersion = "4.2.1";