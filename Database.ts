import {BelongsToMany, DCSerializable, ForeignColumn, Column, Table, HasMany} from '@devctrl/data-model';
import {ApplicationDatabase} from './ApplicationDatabase';
import {Application} from './Application';
import {Machine} from './Machine';
import {Note} from "./Note";

@Table
export class Database extends DCSerializable {
  @Column({
    label: "Size (MB)",
    readonly: true
  }) size: number;
  @BelongsToMany(() => Application, () => ApplicationDatabase) applications: Application[];
  @ForeignColumn(() => Machine) host: Machine;
  @HasMany(() => Note) notes: Note[];

  fkSelectName(): string {
    return `${this.name} (${this.host.name})`;
  }
}
