import {
  DCSerializable,
  ForeignColumn,
  Table,
  HasMany, Column
} from '@devctrl/data-model';
import {IPAddress} from './IPAddress';
import {Application} from './Application';
import {Note} from "./Note";


@Table
export class Hostname extends DCSerializable {
  @ForeignColumn(() => IPAddress, true) internalIpAddress: IPAddress;
  @ForeignColumn(() => IPAddress, true) externalIpAddress: IPAddress;
  @HasMany(() => Application) applications: Application[];
  @HasMany(() => Note) notes: Note[];
  @Column({
    label: "Shibboleth SP",
    tooltip: "Shibboleth service provider URN",
    optional: true
  }) shibSp: string;
}
