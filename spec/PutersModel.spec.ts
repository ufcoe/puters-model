import {DCDataModel} from '@devctrl/data-model';
import {PutersModelList} from '../index';

describe('The Puters data model', function() {
  it('should be a valid data model', function() {
    let model = new DCDataModel(PutersModelList);

    expect(Object.values(model.schema).length).toBe(10);
  });
});