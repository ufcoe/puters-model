import {
  DCSerializable,
  ForeignColumn,
  Table,
  HasMany
} from '@devctrl/data-model';
import {Machine} from './Machine';
import {Hostname} from './Hostname';
import {Note} from "./Note";

@Table
export class IPAddress extends DCSerializable {
  @ForeignColumn(() => Machine, true) machine: Machine;
  @HasMany(() => Hostname, 'internalIpAddress') internalHostnames: Hostname[];
  @HasMany(() => Hostname, 'externalIpAddress') externalHostnames: Hostname[];
  @HasMany(() => Note) notes: Note[];
}
